{
  // +:: is important (we don't want to override the
  // _config object, just add to it)
  _config+:: {
    // define a namespace for this library
    jaeger: {
      jaeger: {
        port: 3000,
        name: "jaeger",
      },
      prometheus: {
        port: 9090,
        name: "prometheus"
      }
    }
  },

  // again, make sure to use +::
  _images+:: {
    jaeger: {
      image: "jaegertracing/jaeger-operator",
      tag: "1.19.0",
    }
  }
}