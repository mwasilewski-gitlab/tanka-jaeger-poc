(import "ksonnet-util/kausal.libsonnet") +
(import "./config.libsonnet") +
// (import "./jaegertracing.io_jaegers_crd.yaml.libsonnet") +
{
  local deployment = $.apps.v1.deployment,
  local container = $.core.v1.container,
  local port = $.core.v1.containerPort,
  local service = $.core.v1.service,

  // used in jaeger
  local namespace = $.core.v1.namespace,
  local clusterRole= $.rbac.v1.clusterRole,

  // alias our params, too long to type every time
  local c = $._config.promgrafana,

  jaeger: {
    namespace: namespace.new(
      name="jaeger",
    ),
    clusterRole: clusterRole.new(
      name="jaeger-role",

    ),


  }
}
